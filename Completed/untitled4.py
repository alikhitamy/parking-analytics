
import pandas as pd
import datetime
import openpyxl


def ascend(lt):
    new=[]
    for a in range(1,13):
        for b in range(0,len(lt)):
            if str(a) in lt[b].split('/')[1]:
                
                new+=[lt[b]]
    return (new)
            
def to_excel(array,rw,sheet):
     # this is a for loop to fill up the column
    for a in range(0,len(array)):
        sheet.cell(row=rw,column=a+1).value=array[a]
    
wb=openpyxl.Workbook()
sheet=wb.active



df=pd.read_csv('merged_ZoneB_Charges.csv')

df2=(df.sort_values(by='Date'))

df2.index = range(len(df2.index))


lt=df2['Date'].unique()
lt=ascend(lt)

lo=datetime.datetime(1900,1,1,8,00,00)
up=datetime.datetime(1900,1,1,11,00,00)
df2['Time'] = df2['Time'].str.replace(' ','')
df2['Time'] = pd.to_datetime(df2['Time'],format='%H:%M:%S')


inter=[]
for b in range(0,24,2):
    if b+2!=24:
        inter+=[(datetime.datetime(1900,1,1,b,00,00),datetime.datetime(1900,1,1,(b+2),00,00))]
    else:
        inter+=[(datetime.datetime(1900,1,1,b,00,00),datetime.datetime(1900,1,1,23,59,59))]

count=1  
for a in range(0,len(lt)):
    vl=[lt[a],'Daily','Season','Total']
    to_excel(vl,count,sheet)
    count+=1
    for c in range(0,len(inter)):
        tim=(df2.loc[(df2['Date']==lt[a]) & (df2['Time']>=inter[c][0]) & (df2['Time']<inter[c][1])])
        
        #tim=(dt.loc[(dt['Time']>=lo) & (dt['Time']<up)])
        
        daily=(tim.loc[tim['User_Type']=='Daily' ])

        # this is returning a list of dialy entries
        daily_entry=daily.loc[daily['Action']=='Entry' ]['Studentid'].tolist()
        daily_exit=(daily.loc[daily['Action']=='Exit' ])['Studentid'].tolist()

        # ml is the list of number of cars Entered intersected by the number of car exited
        m1=set(daily_entry).intersection(daily_exit)

        # total number of cars using the parking is tl
        #The method len() returns the number of elements in the list.

        t1=len(daily_entry)-len(m1)

        #loc is to Access a group of rows and columns by label(s)
        
        season=(tim.loc[tim['User_Type']=='Season' ])
        
        season_entry=(season.loc[season['Action']=='Entry' ])['Studentid'].tolist()
        season_exit=(season.loc[season['Action']=='Exit' ])['Studentid'].tolist()
        m2=set(season_entry).intersection(season_exit)
        t2=len(season_entry)-len(m2)
        time_interval=(str(inter[c][0])+'-'+str(inter[c][1])).replace('1900-01-01','')
        vl=[time_interval,t1,t2,t1+t2]
        to_excel(vl,count,sheet)
        count+=1
        
    vl=['']*4
    to_excel(vl,count,sheet)
    count+=1   
    
wb.save('output.xlsx')

